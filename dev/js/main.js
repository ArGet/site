
// "class" - select class
// ""  - do nothing, delay
// " " - clear all selected objects
var homePhraseArray = ["", "", " ", "", "we", "are", "arget", "", "", " ", "", "we", "are", "great", "team", "", "", " ", "", "we", "make", "great", "projects", ""];
var TIME_DELAY_ANIMATION_TEXT = 600;
var $ripples;


$(document).ready(function(){

	animationHomeText();
	initFormEmail();
})


function initFormEmail() {

	$ripples = $('.ripples');

	$('input').blur(function() {inputExit($(this));});
	$ripples.mouseout(function() {btnSendMouseOut();});
	$ripples.mousedown(function(e) {btnSendMouseDown(e);});
	$ripples.mouseup(function(e) {btnSendMouseUp(e);});
	$ripples.click(function(e) {btnSendClick(e);});
}

function inputExit(obj) { console.log("inputExit");

	var $this = obj;
	if ($this.val())
		$this.addClass('used');
	else
		$this.removeClass('used');
}

function btnSendClick(e) { console.log("btnSendClick");

	clearTimeout(addedPressed);
	$(this).removeClass('is-pressed');
}

function btnSendMouseUp(e) { console.log("mouseup");
	$ripples.removeClass('is-pressed');
}

function btnSendMouseDown(e) { console.log("mousedown");

	var $this = $ripples;
	var $offset = $this.parent().offset();
	var $circle = $this.find('.ripplesCircle');

	var x = e.pageX - $offset.left;
	var y = e.pageY - $offset.top;

	$circle.css({
		top: y + 'px',
		left: x + 'px'
	});

	$this.addClass('is-active');

	setTimeout( function() {
		$this.removeClass('is-active');
	}, 250);

	addedPressed = setTimeout( function() {
		$this.addClass('is-pressed');
	}, 200);
}

function btnSendMouseOut() { console.log("mouseout");

	$('.ripples').removeClass('is-pressed');
	if (typeof addedPressed != 'undefined') {
		clearTimeout(addedPressed);
	}
}

function animationHomeText() {

	setTimeout( function() {

		for (var i = 0; i < homePhraseArray.length; i++) {
			selected(i);
		}

		setTimeout( function() {
			animationHomeText();
		}, homePhraseArray.length * TIME_DELAY_ANIMATION_TEXT);
	}, 1000);
}

function selected(number) {

	setTimeout( function() {

		object = homePhraseArray[number];
		// console.log("add" + object);

		if (object == "") {
			// nothing
		} else if (object == " ") {
			clearSelectedText();
		} else {
			$(".home-phrase-" + object).addClass("animation-text-selection");
		};
		
	}, (number + 1) * TIME_DELAY_ANIMATION_TEXT);
}

function clearSelectedText() { // console.log("--clear");

	for (var i = 0; i < homePhraseArray.length; i++) {
		$(".home-phrase-" + homePhraseArray[i]).removeClass("animation-text-selection");
	}
}